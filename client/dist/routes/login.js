'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.routes = routes;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// promise based

function routes(router) {
    router.post('/login', async (req, res) => {
        try {
            console.log(`CLIENT - POST input to /login - req.body.email: ${JSON.stringify(req.body.email)}`);
            const response = await (0, _axios2.default)({
                method: 'get',
                url: `http://server:5000/v1/user/${req.body.email}`
                // url: `http://localhost:5000/v1/user/${req.body.email}`,
                // url: `http://172.20.0.3:5000/v1/user/${req.body.email}`,
            });
            console.log(`CLIENT - POST output of /login - response.data: ${JSON.stringify(response.data)}`);
            res.json(response.data);
        } catch (err) {
            console.error(new Error(err));
        }
    });
}

// ISSUES:
// DOES NOT CHECK INPUTS (password)
// DOES NOT CHECK OUTPUT 
// DOES NOT RETURN STATUS CODES FOR CORRESPONDING RESPONSES
//# sourceMappingURL=login.js.map