'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.routes = routes;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function routes(router) {
    router.post('/register', async (req, res) => {
        const { name, address, password, email } = req.body;
        console.log(`S1 input to /register: ${JSON.stringify(req.body)}`);
        try {
            let response = await (0, _axios2.default)({
                method: 'post',
                url: `http://server:5000/v1/register`,
                // url: `http://localhost:5000/v1/register`,
                // url: `http://172.19.0.4:5000/v1/register`,
                data: {
                    name,
                    address,
                    password,
                    email
                }
            });
            console.log(response.data);
            res.send(response.data);
        } catch (err) {
            console.error(new Error(err));
        }
    });
} // THIS ROUTE ISN'T BEING USED YET

// import { postgres } from '../postgres';
// import { insert } from '../model';
//# sourceMappingURL=account.js.map