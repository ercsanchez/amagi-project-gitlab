'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.routes = routes;

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function routes(router) {
    router.post('/register', async (req, res) => {
        const { first_name, address, password, email } = req.body;
        console.log(`CLIENT - POST input to /register - req.body: ${JSON.stringify(req.body)}`);
        try {
            let response = await (0, _axios2.default)({
                method: 'post',
                url: `http://server:5000/v1/register`,
                // url: `http://localhost:5000/v1/register`,
                // url: `http://172.20.0.3:5000/v1/register`,
                data: {
                    first_name,
                    address,
                    password,
                    email
                }
            });
            console.log(`CLIENT - POST output of /register - response.data: ${JSON.stringify(response.data)}`);
            res.json(response.data);
        } catch (err) {
            console.error(new Error(err));
        }
    });
}

// ISSUES:
// DOES NOT CHECK IF EMAIL ALREADY EXISTS
// DOES NOT CHECK INPUTS
// DOES NOT CHECK OUTPUT
// DOES NOT RETURN STATUS CODES FOR CORRESPONDING RESPONSES
//# sourceMappingURL=register.js.map