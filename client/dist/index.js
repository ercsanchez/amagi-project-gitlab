'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _swaggerUiExpress = require('swagger-ui-express');

var _swaggerUiExpress2 = _interopRequireDefault(_swaggerUiExpress);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _swagger = require('swagger2');

var swagger = _interopRequireWildcard(_swagger);

var _register = require('./routes/register');

var _login = require('./routes/login');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import { postgresMiddleware } from './postgres'
// import { schema } from './model'

const app = (0, _express2.default)();
const router = _express2.default.Router();

app.use((0, _bodyParser2.default)());
//.use(postgresMiddleware(schema));

const spec = swagger.loadDocumentSync('./src/swagger.yaml');

if (!swagger.validateDocument(spec)) {
    throw Error(`Invalid Swagger File`);
}

for (const routes of [_register.routes, _login.routes]) {
    routes(router);
}

// SWAGGER
app.use('/', _swaggerUiExpress2.default.serve);
app.get('/', _swaggerUiExpress2.default.setup(spec));
router.get('/swagger.json', (req, res) => {
    res.send(spec);
});

app.use('/v1', router);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`client listening on ${PORT}`));
//# sourceMappingURL=index.js.map